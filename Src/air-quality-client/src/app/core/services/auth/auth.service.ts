import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private httpClient: HttpClient) { }

  getAccessToken(username: string, password:string){
    return this.httpClient.post(
      `${environment.secure ? 'https' : 'http'}://${environment.host}/auth/login`,
      {
        username, password
      }
    )
  }

  logout(){}

  register(){}
}
