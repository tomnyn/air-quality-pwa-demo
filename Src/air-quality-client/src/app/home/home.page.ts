import { Component } from '@angular/core';
import { AuthService } from '../core/services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private authService: AuthService) {
    this.login()
  }

  login(){
    let username = 'john';
    let password = 'changeme';
    this.authService.getAccessToken(
      username, password
    ).subscribe(res => {
      console.log(res);
      localStorage.setItem('username', username);
      localStorage.setItem('credentials', JSON.stringify({
        username, 
        // @ts-ignore
        access_token: res.access_token
      }))
      console.log(JSON.parse(localStorage.getItem('credentials')))
    })
  }

}
